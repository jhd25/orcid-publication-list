<!-- Include ORCID derived publication list using JHD script V3.2 -->
<?php
// PHP to generate a publication list from ORCID given a $orcid, designed tobe included in a page
// Will just fail if ORCID is down

 // Sort function for ordering records by latest year first
 function cmp($a, $b) {
        return $b["year"] - $a["year"];
        }

 // Set up query using JHD API key
 #libxml_use_internal_errors(true);
 $authkey = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
 $URL = "https://pub.orcid.org/v3.0/$orcid/works/";
 $header1 = "Content-Type: application/orcid+xml";
 $header2 = "Authorization: Bearer $authkey";
 $ch = curl_init();
 curl_setopt($ch, CURLOPT_ENCODING , "gzip"); 
 curl_setopt($ch, CURLOPT_URL, $URL);
 curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    $header1,
    $header2
    ));
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

//Now query ORCID for all the work records associated with the provided orcid

 $putcodes=array();
 $j=0;
 foreach (simplexml_load_string(curl_exec($ch))->children('activities',TRUE)->group as $record)
	{
//This code extracts the preferred record for each work using the display index attribute
		$i=0;
		$attr=array();
		foreach ($record->children('work',TRUE)->{'work-summary'} as $subrecord)
		{
			$att='display-index';
			$attr[$i]=(integer)$subrecord->attributes()->$att;
			$i=$i+1;
		}
		$prefs=max($attr);
		$prefsk=array_search($prefs,$attr);
		$att='put-code';
		$putcodests[$j]['code']=(integer)$record->children('work',TRUE)->{'work-summary'}[$prefsk]->attributes()->$att;
	        $putcodests[$j]['year']=(integer)$record->children('work',TRUE)->{'work-summary'}[$prefsk]->children('common',TRUE)->{'publication-date'}->children('common',TRUE)->{'year'};
		$j=$j+1; 
	}

//Sort into date order so that the page splits work correctly
	usort($putcodests, "cmp");
	$putcodes=array_column($putcodests,"code");

//Work out the chunk splits
	$numrecs=50;
	$records=count($putcodes);
	$chunks=floor($records/$numrecs);
	if ($records % $numrecs > 0){$chunks++;}
	$putcodes_chunked=array_chunk($putcodes,$numrecs);

// Work out which chunk is being requested

	if(isset ($_GET['page']))
		{
			$pchunk=(int)$_GET['page'];
		}
	else
		{
			$pchunk=0;
		}
	if (!(($pchunk>=0) && ($pchunk < $chunks)))
	{$pchunk=0;} 
	$startl=($pchunk*$numrecs)+1;

// Display navigation

	if ($chunks>1) {
	echo "<p><em>";
	if ($pchunk!=0){echo "<a href=\"".strtok($_SERVER["REQUEST_URI"],'?')."\">First  </a>";}
	if ($pchunk!=0) {echo " <a href=\"".strtok($_SERVER["REQUEST_URI"],'?')."?page=".($pchunk-1)."\">  Back</a>";}
	echo "  Page ".($pchunk+1)." of $chunks";
	if ($pchunk<($chunks-1)) {echo " <a href=\"".strtok($_SERVER["REQUEST_URI"],'?')."?page=".($pchunk+1)."\">  Forward</a>";}
	if ($pchunk<($chunks-1)) {echo "<a href=\"".strtok($_SERVER["REQUEST_URI"],'?')."?page=".($chunks-1)."\">  Last</a>";}
	echo "</em></p>"; }

// Now output the actual publication list 
	echo '<ol class="publist" start="'.$startl.'">';
	$putcodes_q="";
	$i=0;
	foreach($putcodes_chunked[$pchunk] as $putcode)
		{
		if ($i != 0) {$putcodes_q=$putcodes_q.",";}
		$putcodes_q=$putcodes_q.$putcode; 
		$i++;
		}

// Request ful details for the works in the chunk we are interested in

	curl_setopt($ch, CURLOPT_URL,"https://pub.orcid.org/v3.0/$orcid/works/$putcodes_q");
	$records_w=simplexml_load_string(curl_exec($ch))->children('work',TRUE);
	curl_close($ch);
	$records_z=array();
	$j=0;

//Sort into reverse data order
	foreach($records_w as $record)
		{
		$records_z[$j]['workrec']=$record;
		$records_z[$j]['year']=(integer)$record->children('common',TRUE)->{'publication-date'}->children('common',TRUE)->{'year'};
		$j=$j+1;	
		}
	usort($records_z, "cmp");

//Output a list item for each work

        foreach(array_column($records_z,"workrec") as $record)
		{
		echo "<li>";
		$doi="";
		foreach ($record->children('common',TRUE)->{'external-ids'}->children('common',TRUE)->{'external-id'} as $ceid)
			{
				if ($ceid->children('common',TRUE)->{'external-id-type'}=="doi")
					{
					 $doi=(string)$ceid->children('common',TRUE)->{'external-id-value'};
					};
			};
		if (is_null($record->children('work',TRUE)->contributors->children('work',TRUE)->contributor))
		{$num_authors=0;}
		else {
		$num_authors=$record->children('work',TRUE)->{'contributors'}->children('work',TRUE)->{'contributor'}->count();
		}
		$i=1;
		$author_list="";
		if ($num_authors > 0) {
		foreach ($record->children('work',TRUE)->{'contributors'}->children('work',TRUE)->{'contributor'} as $author)
			{
				$author_list=$author_list.$author->children('work',TRUE)->{'credit-name'};
				if ($i == ($num_authors-1))
				{$author_list=$author_list." and ";}
				else if ($i==$num_authors)
				{}
				else 
				{$author_list=$author_list.", ";}
				$i++;
			}
			}
		$outstring=htmlentities($author_list)."<br/><strong>".htmlentities($record->children('work',TRUE)->{'journal-title'})."</strong><em> (".htmlentities($record->children('common',TRUE)->{'publication-date'}->children('common',TRUE)->{'year'}).")</em><br/><a href=\"http://dx.doi.org/$doi\"><em>".htmlentities(html_entity_decode($record->children('work',TRUE)->{'title'}->children('common',TRUE)->{'title'}))."</em></a>";

// Some output tidying
		$outstring=str_replace('&lt;inf&gt;','<sub>',$outstring);
		$outstring=str_replace('&lt;/inf&gt;','</sub>',$outstring);
		$outstring=str_replace('&amp;amp;','and',$outstring);
		echo $outstring;
		echo "</li>\r\n";

		}

?>
<br/>
</ol>
<?php
//Bottom navigation
        if ($chunks>1) {
        echo "<p><em>";
        if ($pchunk!=0){echo "<a href=\"".strtok($_SERVER["REQUEST_URI"],'?')."\">First  </a>";}
        if ($pchunk!=0) {echo " <a href=\"".strtok($_SERVER["REQUEST_URI"],'?')."?page=".($pchunk-1)."\">  Back</a>";}
        echo "  Page ".($pchunk+1)." of $chunks";
        if ($pchunk<($chunks-1)) {echo " <a href=\"".strtok($_SERVER["REQUEST_URI"],'?')."?page=".($pchunk+1)."\">  Forward</a>";}
        if ($pchunk<($chunks-1)) {echo "<a href=\"".strtok($_SERVER["REQUEST_URI"],'?')."?page=".($chunks-1)."\">  Last</a>";}
        echo "</em></p>"; }
?>
