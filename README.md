# ORCID publication list

This PHP script downloads and presents an author's bibliography from ORCID
It expects the PHP variable $orcid to be set appropriatly
An authkey needs to be obtained from ORCID following the instructions here:
https://members.orcid.org/api/tutorial/read-orcid-records#gettoken